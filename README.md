# Verificação de Assinaturas CMS

Este é um exemplo de integração dos serviços da API de verificação com clientes baseados em tecnologia Python.

Este exemplo apresenta os passos necessários para a verificação de assinaturas CMS.

    Para verificar uma assinatura ATTACHED:
        - Passo A1: Carregar o documento assinado.
        - Passo A2: Enviar a requisição para a API de verificação da BRy com o documento carregado.
        - Passo A3: Obtém as informações sobre a assinatura.

    Para verificar uma assinatura DETTACHED:
        - Passo D1: Carregar o arquivo original
        - Passo D2: Carregar a assinatura
        - Passo D3: Enviar a requisição para a API de verificação da BRy com o documento carregado.
        - Passo D4: Obtém as informações sobre a assinatura.
    
    Para verificar uma assinatura HASH
        - Passo H1: Carregar a assinatura
        - Passo H2: Inserir o Hash do documento original
        - Passo H3: Enviar a requisição para a API de verificação da BRy com o hash do documento
        - Passo H4: Obtém as informações sobre a assinatura.

### Tech

O exemplo utiliza das bibliotecas Python abaixo:
* [Requests] - Elegant and simple HTTP library for Python, built for human beings.
* [Python3.6] - Python 3.6

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a verificação da assinatura.

| Variável | Descrição | 
| ------ | ------ |
| AuthToken | Access Token para o consumo do serviço (JWT). |


## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência (utilizamos o Python versão 3.6).

Comandos:

Executar programa:

    -python3 cms_signature_verifier.py


   [Requests]: <https://requests.readthedocs.io/en/master/>
   [Python3.6]: <https://www.python.org/downloads/release/python-360/>

