import requests
import json
import base64
import time


URL_SERVER_CMS = 'https://fw2.bry.com.br/api/cms-verification-service/v1/signatures/verify';

HEADER = {'Authorization': 'Bearer insert-a-valid-token'}

def token_verify():
    aux_authorization = HEADER['Authorization'].split(' ')
    if(aux_authorization[1] == 'insert-a-valid-token'):
        print("Configure a valid token")
        return False
    else:
        return True



def verification_report(result):
    print('Verification JSON response: ',result)
    
    number_of_verified_signatures = len(result['verificationStatus'])

    print()

    print('Number of verified signatures ', number_of_verified_signatures)

    for i in range(number_of_verified_signatures):
        verified_signature = result['verificationStatus'][i]

        print('')
        print('General status: ', verified_signature['generalStatus'])

        print('Signature format: ', verified_signature['signatureFormat'])

        verified_signature_status = verified_signature['signatureStatus'][0]

        try:
            print(verified_signature_status['signaturePolicyStatus'])

            print('Signature status: ', verified_signature_status['status'])

            # print('Base64 of the original document hash: ', verified_signature_status['originalFileBase64Hash'])

            print('Signature date: ', verified_signature_status['signingTime'])

            print('Verification reference type: ', verified_signature_status['verificationReferenceType'])

            print('')

        except KeyError:
            print('')
        
        

        try:
            
            time_stamp_status_of_signature = verified_signature_status['signatureTimeStampStatus']
            if len(time_stamp_status_of_signature) > 0:
                print('Time stamp information: ')
                certificate_time_stamper_index = len(time_stamp_status_of_signature[0]['timestampChainStatus']['certificateStatusList']) - 1
                time_stamper_certificate = time_stamp_status_of_signature[0]['timestampChainStatus']['certificateStatusList'][certificate_time_stamper_index]
                print('Time stamper: ', time_stamper_certificate['certificateInfo']['subjectDN']['cn'] if time_stamper_certificate else ' ')
                print('Time stamp status of signature: ', time_stamp_status_of_signature[0]['status'])
                print('Time stamp date: ', time_stamp_status_of_signature[0]['timeStampDate'])
                print('Verification reference date : ', time_stamp_status_of_signature[0]['verificationReference'])
                print('Verification reference type : ', time_stamp_status_of_signature[0]['verificationReferenceType'])
                print('Time stamp policy: ', time_stamp_status_of_signature[0]['timeStampPolicy'])
                print('Time stamp content hash: ', time_stamp_status_of_signature[0]['timeStampContentHash'])
                print('Time stamp serial number : ', time_stamp_status_of_signature[0]['timestampSerialNumber'])
                print(' ')
        except KeyError:
            print(' ')

        signer_chain_status = verified_signature_status['chainStatus']
        signer_certificate_index = len(signer_chain_status['certificateStatusList']) - 1
        signer_certificate = signer_chain_status['certificateStatusList'][signer_certificate_index]

        print('Signer information: ')
        try:
            print('Signer: ', signer_certificate['certificateInfo']['subjectDN']['cn'])
            print('General status of the certificate chain: ', signer_chain_status['status'])
            print('Signer certificate status ', signer_certificate['status'])
            print('ICP-Brasil certificate: ', signer_certificate['pkiBrazil'])
            print('Initial validity date of the signer certificate: ', signer_certificate['certificateInfo']['validity']['notBefore'])
            print('Final date of validity of the signer certificate: ', signer_certificate['certificateInfo']['validity']['notAfter'])
        except KeyError:
            print('Incomplete chain of signer: Could not verify signer certificate')
            print('General status of the signer chain: ', signer_chain_status['status'])

def verify_cAdES_attached_signature():
    # Step A1 - Upload the signature to be verified
    file = open('./signatures/attached/AssinaturaAttachedADRC.p7s', 'rb').read()
    signatures = []
    signatures.append(("signatures[0][content]", file))
    verification_form = {
        'nonce': 1,
        'signatures[0][nonce]' : 1,
    }

    print('====================== Starting verification of the CAdES attached signature ... =======================')
    # Step A2 - Send the request to the BRy verification API with the uploaded document.
    response = requests.post(URL_SERVER_CMS, data=verification_form, files=signatures, headers=HEADER)
    time.sleep(2)
    if response.status_code == 200:
        # Step A3 - Get the signatures information
        verification_report(response.json())
    else:
        print(response.text)

def verify_cAdES_detached_signature():

    # Step D1 - Upload the original file

    original_doc = open('./signatures/detached/originalfile.txt', 'rb').read()
    # Step D2 - Upload signature

    signed_doc = open('./signatures/detached/signature.p7s', 'rb').read()

    signatures = []
    signatures.append(('signatures[0][content]', original_doc))
    signatures.append(('signatures[0][documentContent]', signed_doc))

    verification_form = {
        'nonce': 1,
        'signatures[0][nonce]': 1
    }

    print('================== Starting verification of the CAdES detached signature ... ====================')
    # Step 3 - Send the request to the BRy verification API with the original document and signature.
    response = requests.post(URL_SERVER_CMS, data=verification_form, files=signatures, headers=HEADER)
    time.sleep(2)

    if response.status_code == 200:        
        # Step 4 - Gets the information about the signature.
        verification_report(response.json())
    else:
        print(response.text)



def verify_cAdES_detached_signature_sending_orignal_document_hash():

    # Step H1 - Upload signature

    signed_doc = open('./signatures/detached/signature.p7s', 'rb').read()

    signatures = []
    signatures.append(('signatures[0][content]', signed_doc))

    verification_form = {
        'nonce': 1,
        'signatures[0][nonce]': 1,
        # Step H2 - Insert hash of the original document
        'signatures[0][documentHashes][0]': '6C584E4E00C9A0BD7B81CE6A4176A77D627834B54340E1898CC735BECB04AD3F'
    }

    print('=== Starting CadEs detached verification signature sending orignal document hash ...===')
    # Step H3 - Send the request to the BRy verification API with the document hash.
    response = requests.post(URL_SERVER_CMS, data=verification_form, files=signatures, headers=HEADER)
    time.sleep(2)
    if response.status_code == 200:
        # Step H4 - Gets the information about the signature.

        verification_report(response.json())
    else:
        print(response.text)

if token_verify():
    verify_cAdES_attached_signature()
    verify_cAdES_detached_signature_sending_orignal_document_hash()
    verify_cAdES_detached_signature()